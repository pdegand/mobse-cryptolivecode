package com.octo.livecodecrypto

import android.os.Bundle
import android.security.keystore.KeyGenParameterSpec
import android.security.keystore.KeyProperties.*
import android.util.Base64
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.security.crypto.EncryptedSharedPreferences
import androidx.security.crypto.MasterKeys
import java.security.*
import javax.crypto.Cipher

class MainActivity : AppCompatActivity() {

    companion object {
        private const val KEYSTORE_PROVIDER = "AndroidKeyStore"
        private const val DEFAULT_KEY_ALIAS = "key"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        testSelfCryptedPassword()
        testEncryptedSharedPreferences()
    }

    // ==========================================================================
    // SelfCryptedPassword
    // ==========================================================================

    private fun testSelfCryptedPassword() {
        // Générer / charger les clés de cryptage

        // Crypte une donnée

        // Stocke la donnée

        // Récupérer la donnée

        // Décrypter la donnée

        // Afficher la donnée décryptée
    }

    // ==========================================================================
    // EncryptedSharedPreferences
    // ==========================================================================

    private fun testEncryptedSharedPreferences() {
        // Stocker une donner dans une SharedPreference sécurisée
    }

    // ==========================================================================
    // Générer / charger les clés de cryptage
    // ==========================================================================

    private fun generateOrFetchKeys(): KeyPair {
        // si fetchKeys() est null, return generateKeys() sinon return la valeur de fetchKeys()
        return fetchKeys() ?: generateKeys()
    }

    private fun generateKeys(): KeyPair {
        val keyPairGenerator = KeyPairGenerator.getInstance(KEY_ALGORITHM_RSA, KEYSTORE_PROVIDER)
        val keyPairGeneratorBuilder = KeyGenParameterSpec
            .Builder(
                DEFAULT_KEY_ALIAS,
                PURPOSE_ENCRYPT or PURPOSE_DECRYPT
            )
            .setBlockModes(BLOCK_MODE_ECB)
            .setEncryptionPaddings(ENCRYPTION_PADDING_RSA_PKCS1)
        keyPairGenerator.initialize(keyPairGeneratorBuilder.build())

        return keyPairGenerator.generateKeyPair()
    }

    private fun fetchKeys(): KeyPair? {
        val keyStore = KeyStore.getInstance(KEYSTORE_PROVIDER)
        keyStore.load(null)

        return if (keyStore.containsAlias(DEFAULT_KEY_ALIAS)) {
            try {
                KeyPair(
                    keyStore.getCertificate(DEFAULT_KEY_ALIAS).publicKey,
                    keyStore.getKey(DEFAULT_KEY_ALIAS, null) as PrivateKey
                )
            } catch (e: Exception) {
                null
            }
        } else {
            null
        }
    }

    // ==========================================================================
    // Crypter / décrypter une donnée
    // ==========================================================================

    private fun getCipher(): Cipher {
        return Cipher.getInstance("$KEY_ALGORITHM_RSA/$BLOCK_MODE_ECB/$ENCRYPTION_PADDING_RSA_PKCS1")
    }

    private fun cryptData(data: String, publicKey: PublicKey): String {
        val cipher = getCipher()

        cipher.init(Cipher.ENCRYPT_MODE, publicKey)
        return String(Base64.encode(cipher.doFinal(data.toByteArray()), Base64.DEFAULT))
    }

    private fun decryptData(data: String, privateKey: PrivateKey): String {
        val cipher = getCipher()

        cipher.init(Cipher.DECRYPT_MODE, privateKey)
        return String(cipher.doFinal(Base64.decode(data.toByteArray(), Base64.DEFAULT)))
    }

}